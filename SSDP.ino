void SSDP_init(JsonObject& deviceConfig) {
  if (!serviceIsEnabled(deviceConfig, "SSDP")) return;
  
  JsonObject& ssdpConfig =  deviceConfig["SSDP"];
  int ssdpPort = deviceConfig["HTTP"]["httpPort"].as<uint32_t>();
  
  HTTP->on(const_cast<char *>(("/" + ssdpConfig["schemaURL"].as<String>()).c_str()), HTTP_GET, []() {
    SSDP.schema(HTTP->client());
  });

  //Если версия  2.0.0 закаментируйте следующую строчку
  //SSDP.setDeviceType("upnp:rootdevice");
  // urn:schemas-upnp-org:device:Basic:1

  SSDP.setDeviceType("upnp:rootdevice");
  SSDP.setSchemaURL(ssdpConfig["schemaURL"].as<String>());
  SSDP.setHTTPPort(ssdpPort);
  SSDP.setName(ssdpConfig["name"].as<String>());
  SSDP.setSerialNumber(ssdpConfig["serialNumber"].as<String>());
  SSDP.setURL(ssdpConfig["url"].as<String>());
  SSDP.setModelName(ssdpConfig["modelName"].as<String>());
  SSDP.setModelNumber(ssdpConfig["modelNumber"].as<String>());
  SSDP.setModelURL(ssdpConfig["modelURL"].as<String>());
  SSDP.setManufacturer(ssdpConfig["manufacturer"].as<String>());
  SSDP.setManufacturerURL(ssdpConfig["manufacturerURL"].as<String>());
  SSDP.begin();
  
  bootLog += "Started SSDP.\n";  
  Serial.println("Started SSDP");
}
