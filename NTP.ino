void NTP_init(JsonObject& deviceConfigLocal) {
  if (!serviceIsEnabled(deviceConfig, "NTP")) return;
  
  static WiFiEventHandler e1, e2;
  JsonObject& ntpConfig =  deviceConfig["NTP"];
  const char* ntpServerName = ntpConfig["timeServer"];


  NTP.begin(ntpServerName, ntpConfig["timeOffset"].as<int>(), true);
  NTP.setInterval(63);
  
  Serial.printf("Started NTP. Time Server : %s.\n", ntpServerName);
}

void NTP_loop(JsonObject& deviceConfig) {
  if (!serviceIsEnabled(deviceConfig, "NTP")) return;

  if (WiFi.isConnected()) return;
  static int i = 0;
  static int last = 0;

  if (syncEventTriggered) {
    processSyncEvent(ntpEvent);
    syncEventTriggered = false;
  }
  
  if ((millis() - last) > 5100) {
    //Serial.println(millis() - last);
    last = millis();
    /*Serial.print(i); Serial.print(" ");
    Serial.print(NTP.getTimeDateString()); Serial.print(" ");
    Serial.print(NTP.isSummerTime() ? "Summer Time. " : "Winter Time. ");
    Serial.print("WiFi is ");
    Serial.print(WiFi.isConnected() ? "connected" : "not connected"); Serial.print(". ");
    Serial.print("Uptime: ");
    Serial.print(NTP.getUptimeString()); Serial.print(" since ");
    Serial.println(NTP.getTimeDateString(NTP.getFirstSync()).c_str());*/

    i++;
  }
  delay(0);
}


void processSyncEvent(NTPSyncEvent_t ntpEvent) {
  if (ntpEvent) {
    Serial.print("Time Sync error: ");
    if (ntpEvent == noResponse)
      Serial.println("NTP server not reachable");
    else if (ntpEvent == invalidAddress)
      Serial.println("Invalid NTP server address");
  }
  else {
    Serial.print("Got NTP time: ");
    Serial.println(NTP.getTimeDateString(NTP.getLastNTPSync()));
  }
}

