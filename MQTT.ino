void MQTT_init(JsonObject& deviceConfig) {
  if (!serviceIsEnabled(deviceConfig, "MQTT")) return;
  
  JsonObject& mqttConfig =  deviceConfig["MQTT"];
  
  MQTT.setServer(const_cast<char *>(mqttConfig["brokerServer"].as<String>().c_str()), mqttConfig["brokerServerPort"].as<uint32_t>());
  MQTT.setCallback(mqttCallback);
  Serial.println("Started MQTT Client");
}

void MQTT_Loop(JsonObject& deviceConfig) {
  if (!serviceIsEnabled(deviceConfig, "MQTT")) return;
  
  JsonObject& mqttConfig =  deviceConfig["MQTT"];
  byte attempts = 11;
  
  if (!MQTT.connected()) {
    while (--attempts && !MQTT.connected()) {
      Serial.print("Attempting MQTT connection...");
      if (MQTT.connect(mqttConfig["clientId"], mqttConfig["clientUser"], mqttConfig["clientPass"])) {
        Serial.println("connected");
        //publish and subscribe after reconnect
        MQTT.publish("outTopic", mqttConfig["clientId"]);
        MQTT.subscribe("inTopic");
      } else {
        Serial.print("failed, rc=");
        Serial.print(MQTT.state());
        Serial.println(" try again in 5 seconds");
        delay(5000);
      }
    }
    if (!MQTT.connected()) mqttConfig["enabled"] = false;
  }
  MQTT.loop();
}

void mqttCallback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}
