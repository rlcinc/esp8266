/*
 * Научиться читать напряжение и в случае его падения ниже какого-то значения отправлять событие на брокер(MQTT).
 * [*] Нужно сделать авторизацию на получение и сохранение конфига. На получение информации об устройстве. Все должно работать через REST c авторизацией
 * [+] Сделать загрузку обновления через веб интерфейс
 * 
 * [*] Пофиксить баг с первоначальной загрузкой конфигурации.
 * [*] При загрузке МЕНЮ сделать автоматическую загрузку первого таба.
 * [*] Довести до ума файловый браузер
 * [+] Сделать табы для отображения DeviceInfo, FsInfo и т.д
 * [*] Рефакторинг файлового браузера
 * [*] Добавить обработку ошибок для Веб морды. Смотри fbrowser. <div slds-Message type="action.type" text="action.text"></div>
 * [*] Добавить поддержку gzip на стороне клиента
 * [+] Добавить upload файлов из file browser
 * [*] Исправить CSS спинера
 * [*] Ctrl+s в редакторе для сохранения
 * [*] все ноды должны быть закрыты
 * [*] сделать поиск по дереву
 * [+] сделать поддержку i18n
 * [+] сделать опрос роутера и получение списка активных устройств
 * [+] сделать проверку на правильность введенного пароля от точки доступа
 * [+] сделать открытие файла в новом табе
 * [+] сделать сохранение всего диска через браузер
 */

#include <FS.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h> 
//#include <ESPhttpUpdate.h>
#include <ESP8266SSDP.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>
#include <TimeLib.h>
#include <NtpClientLib.h>
#include <config.h>
#include <rlc.h>

JsonObject& deviceConfig = jsonBuffer.createObject();

ESP8266WebServer *HTTP; 

WiFiClient espClient;
PubSubClient MQTT(espClient);
WiFiUDP zigbeeUdp;
WiFiUDP ntpUDP;

#define _VERSION "0.4 dev"

void setup() {
  Serial.begin(115200);
  
  loadDeviceConfig(_DEVICE_CONFIG_FILE, deviceConfig);
  const char* curSSID = deviceConfig["WIFI"]["ssid"];
  deviceConfig["General"]["imageDateTime"] = __DATE__ " " __TIME__;
  deviceConfig["General"]["imageFile"] = __FILE__;
  deviceConfig["General"]["version"] = _VERSION;
  HTTP = new ESP8266WebServer(deviceConfig["HTTP"]["httpPort"].as<uint32_t>());
  
  Serial.println(); 
  Serial.println("Starting SmartNet... " + deviceConfig["General"]["version"].as<String>());
  
  
  if (!deviceConfig.containsKey("WIFI") || deviceConfig["WIFI"]["ssid"]=="") {
    Serial.println("Don't found configuration!");
    WIFIStartAsAccessPoint(deviceConfig);
  } else {
    Serial.println("Found configuration!");
    WIFIStartAsClient(deviceConfig);
  }
  zigbeeUdp_init(deviceConfig);
  SSDP_init(deviceConfig);

  HTTPWEB_init(deviceConfig);
  NTP_init(deviceConfig);
  writeLog(deviceConfig, deviceConfig["General"]["log"].as<String>(), bootLog, "boot");
}

void loop() {
  zigbeeUdp_loop(deviceConfig);
  HTTPWEB_loop(deviceConfig);
  MQTT_Loop(deviceConfig);
  NTP_loop(deviceConfig);
}
