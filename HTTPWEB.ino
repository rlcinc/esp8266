File fsUploadFile;

class httpHandler : public RequestHandler {
    bool canHandle(HTTPMethod method, String uri) {
      return true;
    }
    bool canUpload(String uri) {
      return true;
    }
    bool handle(ESP8266WebServer& server, HTTPMethod requestMethod, String requestUri) {
      handleFile(server);
      return true;
    }
    void upload(ESP8266WebServer& server, String requestUri, HTTPUpload& upload) {
      bool isFirmware = (requestUri == deviceConfig["HTTP"]["updatePath"]);
      switch (upload.status) {
        case UPLOAD_FILE_START : {
            if (isFirmware) {
              Serial.setDebugOutput(true);
              WiFiUDP::stopAll();
              uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
              if (!Update.begin(maxSketchSpace)) Update.printError(Serial);
            } else {
              String fName = upload.filename.startsWith("/") ? upload.filename : "/" + upload.filename;
              fName.replace(" ", "_");
              fsUploadFile = SPIFFS.open(fName, "w");
            }
            break;
          }
        case UPLOAD_FILE_END : {
            if (isFirmware) {
              if (Update.end(true)) Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
              else Update.printError(Serial);
              Serial.setDebugOutput(false);
            } else {
              if (fsUploadFile) fsUploadFile.close();
              Serial.println("Uploaded file: " + upload.filename + ". Size: " + (int)upload.totalSize);
            }
            break;
          }
        case UPLOAD_FILE_WRITE : {
            if (isFirmware) {
              if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) Update.printError(Serial);
            } else {
              if (fsUploadFile) fsUploadFile.write(upload.buf, upload.currentSize);
            }
            break;
          }
        case UPLOAD_FILE_ABORTED : {
            if (isFirmware) Update.end();
            Serial.println("Update was aborted");
            break;
          }
      }
      yield();
    }

} HTTPHandler;



void HTTPWEB_init(JsonObject& deviceConfigLocal) {
  if (!serviceIsEnabled(deviceConfigLocal, "HTTP")) return;

  HTTP->on("/config/variables", jsVars);
  HTTP->on("/config/fileslist", fileList);
  HTTP->on("/config/deviceinfo", deviceInfo);
  HTTP->on("/config/fsinfo", fsInfo);
  HTTP->on("/config/wifilist", wifiNetworks);
  HTTP->on("/cmd/udp", sendUdp);

  HTTP->addHandler(&HTTPHandler);

  HTTP->begin();
  bootLog += "Started HTTP.\n";
  Serial.println("Started HTTP");

  const char* path = deviceConfigLocal["HTTP"]["updatePath"];
  bootLog += "Started HTTPUpdateServer.\n";
  Serial.printf("Started HTTPUpdateServer.! Open %s in your browser. You must be authentificated.\n", path);
}

void HTTPWEB_loop(JsonObject& deviceConfig) {
  if (!serviceIsEnabled(deviceConfig, "HTTP")) return;

  HTTP->handleClient();
}

String getContentType(String filename) {
  JsonObject& mimeConfig = deviceConfig["HTTP"]["mimeTypes"];
  String result = "text/plain";
  String ext = filename.substring(filename.lastIndexOf('.') + 1);
  Serial.println("Ext : " + ext);
  if (HTTP->hasArg("download")) result = "application/octet-stream";
  else result = mimeConfig.get<String>(ext.c_str());
  //temporary workaround
  if (ext == "svg") result = "image/svg+xml";

  Serial.print("Content-type for : " + filename + " is ");
  Serial.println(result);
  return result;
}

void handleFile(ESP8266WebServer& HTTP) {
  const char* www_username = deviceConfig["General"]["user"];
  const char* www_password = deviceConfig["General"]["pass"];
  String clientIp = HTTP.client().remoteIP().toString();

  //Serial.print(httpDebug());

  if (HTTP.uri() == "/cfg/config.json") {
    if (!HTTP.authenticate(www_username, www_password)) {
      Serial.println("Bad password from : " + clientIp);
      return HTTP.requestAuthentication();
    }
    Serial.println("Good password from : " + clientIp);
  }

  String formattedTime = NTP.getTimeDateString();
  String logFile = deviceConfig["HTTP"]["log"].as<String>();
  switch (HTTP.method()) {
    case HTTP_GET : {
        if (HTTP.uri() == _DEVICE_CONFIG_FILE) {
          String httpResult = "";
          deviceConfig["General"]["uptime"] = NTP.getUptimeString();
          deviceConfig.prettyPrintTo(httpResult);
          HTTP.send(200, getContentType(HTTP.uri()), httpResult);
        } else {
          if (HTTP.uri() == "/cgi-bin/luci/api/xqsystem/init_info") {
            handleFileRead("/init_info.json");
          } else {
            handleFileRead(HTTP.uri());
          }
        }
        writeLog(deviceConfig, logFile, "[GET] " + HTTP.uri(), formattedTime);
        break;
      }
    case HTTP_POST : {
        Serial.println("HTTP Method is POST");
        HTTP.sendHeader("Connection", "close");
        HTTP.send(200, getContentType(".json"), "{\"status\" : \"ok\"}");
        bool result = handleFileWrite(HTTP.uri(), HTTP.arg("plain"));
        writeLog(deviceConfig, logFile, "[POST] " + HTTP.uri(), formattedTime);
        break;
      }
    case HTTP_PUT : {
        Serial.println("HTTP Method is PUT");
        HTTP.sendHeader("Connection", "close");

        HTTP.send(200, getContentType(".json"), "{\"status\" : \"ok\"}");
        writeLog(deviceConfig, logFile, "[PUT] " + HTTP.uri(), formattedTime);
        if (HTTP.uri() == deviceConfig["HTTP"]["updatePath"]) {
          WiFi.disconnect();
          ESP.restart();
        }
        break;
      }
    case HTTP_DELETE : {
        Serial.println("HTTP Method is DELETE");
        bool result = handleFileDelete(HTTP.uri());
        writeLog(deviceConfig, logFile, "[DELETE] " + HTTP.uri(), formattedTime);
        break;
      }
  }

  if (HTTP.hasArg("needRestart")) {
    Serial.println("Restart ESP");
    writeLog(deviceConfig, logFile, "[RESTART] " + HTTP.uri(), formattedTime);
    WiFi.disconnect();
    ESP.restart();
  }
}

bool handleFileWrite(String path, String line) {
  bool result = false;
  //if (path == "") return false;
  Serial.println("DEBUG : file for save " + path);
  Serial.println(line);

  if (HTTP->uri() == _DEVICE_CONFIG_FILE) {
    JsonObject& jsonCfg = jsonBuffer.parseObject(line);
    if (!jsonCfg.success()) {
      HTTP->send(500, getContentType(".json"), "{\"status\" : \"error. bad input json\"}");
      return false;
    } else {
      for (JsonObject::iterator it = jsonCfg.begin(); it != jsonCfg.end(); ++it) {
        deviceConfig[it->key] = it->value;
      }
    }
  }

  File f = SPIFFS.open(path, "w");
  if (!f) {
    Serial.println("ERROR : file open failed " + path);
    HTTP->send(500, getContentType(".json"), "{\"status\" : \"error. Can't open file\"}");
  } else {
    f.print(line);
    f.close();
    HTTP->sendHeader("Connection", "close");
    HTTP->send(200, getContentType(".json"), "{\"status\" : \"ok\"}");
    result = true;
  }
  return result;
}


bool handleFileRead(String path) {
  bool result = true;
  if (path.endsWith("/")) path += "index.html";
  String contentType = getContentType(path);
  String pathWithGz = path + ".gz";
  if (SPIFFS.exists(pathWithGz)) path = pathWithGz;
  else if (!SPIFFS.exists(path)) {
    path = deviceConfig["HTTP"]["fileNotFound"].as<String>();
    result = false;
  }

  Serial.println("Reading file : " + path);
  File file = SPIFFS.open(path, "r");
  int fileSize = file.size();

  //HTTP.sendHeader("Access-Control-Allow-Origin", "*");

  if (fileSize > 2 * 1024 && !HTTP->hasArg("v")) {
    //HTTP->sendHeader("Last-Modified", deviceConfig["General"]["imageDateTime"].as<String>());
    HTTP->sendHeader("Last-Modified", "Fri, 18 Nov 2016 18:31:51 GMT");
  } else {
    HTTP->sendHeader("Last-Modified", "");
  }

  if (path == deviceConfig["HTTP"]["fileNotFound"] && result == false) {
    HTTP->send(404, contentType, file.readString() );
  } else {
    size_t sent = HTTP->streamFile(file, contentType);
  }
  file.close();
  return result;
}

bool handleFileDelete(String path) {
  bool result = SPIFFS.remove(path);
  if (!result) {
    Serial.println("ERROR : file open failed " + path);
    HTTP->send(500, getContentType(".json"), "{\"status\" : \"error. Can't delete file\"}");
  } else {
    //HTTP->send(200, getContentType(".json"), "{\"status\" : \"ok\"}");
    fsInfo();
  }
  return result;
}
/*
    UTILS PART
*/  

void jsVars() {
  String vars = (String) "var _isAP = " + (_WIFI_CLIENT_MODE == true ? "false" : "true") + ";\n_devName = " + ESP.getChipId();
  HTTP->send(200, getContentType(".js"), vars);
}


void sendUdp() {
  String msg = HTTP->arg("plain");
  Serial.println(msg);
  IPAddress serverIP = convertIpFromString(deviceConfig["ZIGBEEUDP"]["serverIp"].as<String>());
  zigbeeUdp.beginPacket(serverIP, deviceConfig["ZIGBEEUDP"]["serverPort"].as<uint32_t>());
  zigbeeUdp.write(msg.c_str());
  zigbeeUdp.endPacket();
  HTTP->send(200, getContentType(".json"), "{\"status\" : \"ok\"}");
}


void deviceInfo() {
  String httpResult = "";
  DynamicJsonBuffer jsonBufferInt;
  JsonObject& info = jsonBufferInt.createObject();
  info["chipId"] = ESP.getChipId();
  info["flashChipId"] = ESP.getFlashChipId();
  info["flashChipSize"] = ESP.getFlashChipSize();
  info["flashChipRealSize"] = ESP.getFlashChipRealSize();
  info["softAPIP"] = WiFi.softAPIP().toString();
  info["softAPmacAddress"] = WiFi.softAPmacAddress();
  info["macAddress"] = WiFi.macAddress();
  info.printTo(httpResult);
  HTTP->send(200, getContentType(".json"), httpResult);
}

void wifiNetworks() {
  String httpResult = "";
  DynamicJsonBuffer jsonBufferInt;
  int n = WiFi.scanNetworks();
  JsonArray& root = jsonBufferInt.createArray();
  for (int i = 0; i < n; ++i) {
    JsonObject& obj = jsonBufferInt.createObject();
    obj["SSID"] = WiFi.SSID(i);
    obj["RSSI"] = WiFi.RSSI(i);
    obj["BSSID"] = macToStr(WiFi.BSSID(i));
    obj["channel"] = WiFi.channel(i);
    obj["isHidden"] = WiFi.isHidden(i);
    obj["encryptionType"] = getEncryptionType(WiFi.encryptionType(i));
    root.add(obj);
    //delay(1);
    Serial.println(WiFi.SSID(i));
  }
  root.printTo(httpResult);
  HTTP->send(200, getContentType(".json"), httpResult);
}

void fsInfo() {
  String httpResult = "";
  DynamicJsonBuffer jsonBufferInt;
  FSInfo fsInfo;
  SPIFFS.info(fsInfo);
  JsonObject& obj = jsonBufferInt.createObject();
  obj["totalBytes"] = fsInfo.totalBytes;
  obj["usedBytes"] = fsInfo.usedBytes;
  obj["blockSize"] = fsInfo.blockSize;
  obj["pageSize"] = fsInfo.pageSize;
  obj["maxOpenFiles"] = fsInfo.maxOpenFiles;
  obj["maxPathLength"] = fsInfo.maxPathLength;

  obj.printTo(httpResult);
  HTTP->send(200, getContentType(".json"), httpResult);
}

void fileList() {
  String httpResult = "";
  DynamicJsonBuffer jsonBufferInt;
  JsonArray& list = jsonBufferInt.createArray();

  Dir dir = SPIFFS.openDir("");
  while (dir.next()) {
    JsonObject& obj = jsonBufferInt.createObject();
    obj["fName"] = dir.fileName();
    obj["fSize"] = dir.fileSize();
    list.add(obj);
  }
  list.printTo(httpResult);
  HTTP->send(200, getContentType("filelist.json"), httpResult);
}

String httpDebug() {
  String message = "REQUEST : " + HTTP->uri() + "\n\n";
  for ( uint8_t i = 0; i < HTTP->args(); i++ ) {
    message += " " + HTTP->argName ( i ) + ": " + HTTP->arg ( i ) + "\n";
  }
  return message;
}
