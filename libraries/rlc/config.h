#define ONBOARDLED 2

bool syncEventTriggered = false; // True if a time even has been triggered
NTPSyncEvent_t ntpEvent; // Last triggered event

DynamicJsonBuffer jsonBuffer;
bool _WIFI_CLIENT_MODE = false;
String bootLog = "";

const String _DEVICE_CONFIG_FILE = "/cfg/config.json";

const char defaultConfig[] PROGMEM = R"=====(
{
  "General": {
    "user": "admin",
    "pass": "admin",
    "maxLogSize": 100,
    "attempts" : 11,
    "hostName" : "RLCNet",
    "log": "/logs/boot.log",
    "imageDateTime" : null,
    "imageFile" : null,
    "uptime" : null
  },
  "WIFIAP": {
    "ssid": "RLCNet",
    "pass": "wildernet",
    "ip": "192.168.0.100"
  },
  "WIFI": {
    "ssid": "",
    "pass": ""
  },
  "SSDP": {
    "isEnabled": true,
    "schemaURL": "description.xml",
    "httpPort": 80,
    "name": "SmartHome",
    "serialNumber": "00000000",
    "url": "/",
    "modelName": "SmartHome",
    "modelNumber": "v0.1",
    "modelURL": "/cfg/model.html",
    "manufacturer": "Dmitry Rivlin",
    "manufacturerURL": "https:/\/www.linkedin.com/in/salesforcedev401/"
  },
  "mDNS": {
    "isEnabled": true,
    "domain": "test",
	"workOnMode": "WIFICLIENT"
  },
  "MQTT": {
    "isEnabled": false,
    "brokerServer": "192.168.0.1",
    "brokerServerPort": 1883,
    "clientId": "wilderNet",
    "clientUser": "",
    "clientPass": "",
	"workOnMode": "WIFICLIENT"
  },
  "NTP": {
    "isEnabled": true,
    "timeServer" : "time.windows.com",
    "timeOffset" : 2,
	"workOnMode": "WIFICLIENT"
  },
  "ZIGBEEUDP": {
    "isEnabled": true,
    "password" : "test",
	"serverIp" : "192.168.0.116",
    "localPort": 9898,
    "serverPort": 4321,
    "ip": "224.0.0.50",
	"routerPass" : "worker12",
    "log": "/logs/zigbeeUdp.log",
	"workOnMode": "WIFICLIENT"
  },
  "HTTP": {
    "isEnabled": true,
    "httpPort": 80,
    "fileNotFound": "/cfg/404.html",
	"updatePath": "/config/firmware",
  	"log" : "/logs/http.log",
    "mimeTypes": {
      "html": "text/html",
      "log": "plain/text",
      "css": "text/css",
      "json": "application/json",
      "gz": "application/x-gzip",
      "js": "application/javascript",
      "ico": "image/ico",
      "woff2": "font/woff2",
      "svg": "image/svg+xml",
      "png": "image/png",
	  "gif": "image/gif"
    }
  },
  "MenuBuilderAP" : {
    "WiFi List" : "wifilist",
    "xiaomi" : "xiaomi",
    "OTA" : "ota",
    "Config" : "config",
    "DevInfo" : "deviceinfo",
    "FSBrowser" : "fsbrowser"
  }
}
)=====";
