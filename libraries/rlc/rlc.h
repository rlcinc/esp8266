#define ONBOARDLED 2

bool syncEventTriggered = false; // True if a time even has been triggered
NTPSyncEvent_t ntpEvent; // Last triggered event

DynamicJsonBuffer jsonBuffer;
bool _WIFI_CLIENT_MODE = false;
String bootLog = "";

const String _DEVICE_CONFIG_FILE = "/cfg/config.json";

const char defaultConfig[] PROGMEM = R"=====(
{
  "General": {
    "user": "admin",
    "pass": "admin",
    "maxLogSize": 100,
    "attempts" : 11,
    "hostName" : "RLCNet",
    "log": "/logs/boot.log",
    "imageDateTime" : null,
    "imageFile" : null,
    "uptime" : null
  },
  "WIFIAP": {
    "ssid": "RLCNet",
    "pass": "wildernet",
    "ip": "192.168.0.100"
  },
  "WIFI": {
    "ssid": "",
    "pass": ""
  },
  "SSDP": {
    "isEnabled": true,
    "schemaURL": "description.xml",
    "httpPort": 80,
    "name": "SmartHome",
    "serialNumber": "00000000",
    "url": "/",
    "modelName": "SmartHome",
    "modelNumber": "v0.1",
    "modelURL": "/cfg/model.html",
    "manufacturer": "Dmitry Rivlin",
    "manufacturerURL": "https:/\/www.linkedin.com/in/salesforcedev401/"
  },
  "mDNS": {
    "isEnabled": true,
    "domain": "test",
	"workOnMode": "WIFICLIENT"
  },
  "MQTT": {
    "isEnabled": false,
    "brokerServer": "192.168.0.1",
    "brokerServerPort": 1883,
    "clientId": "wilderNet",
    "clientUser": "",
    "clientPass": "",
	"workOnMode": "WIFICLIENT"
  },
  "NTP": {
    "isEnabled": true,
    "timeServer" : "time.windows.com",
    "timeOffset" : 2,
	"workOnMode": "WIFICLIENT"
  },
  "ZIGBEEUDP": {
    "isEnabled": true,
    "password" : "test",
	"serverIp" : "192.168.0.116",
    "localPort": 9898,
    "serverPort": 4321,
    "ip": "224.0.0.50",
	"routerPass" : "worker12",
    "log": "/logs/zigbeeUdp.log",
	"workOnMode": "WIFICLIENT"
  },
  "HTTP": {
    "isEnabled": true,
    "httpPort": 80,
    "fileNotFound": "/cfg/404.html",
	"updatePath": "/config/firmware",
  	"log" : "/logs/http.log",
    "mimeTypes": {
      "html": "text/html",
      "log": "plain/text",
      "css": "text/css",
      "json": "application/json",
      "gz": "application/x-gzip",
      "js": "application/javascript",
      "ico": "image/ico",
      "woff2": "font/woff2",
      "svg": "image/svg+xml",
      "png": "image/png",
	  "gif": "image/gif"
    }
  },
  "MenuBuilderAP" : {
    "WiFi List" : "wifilist",
    "xiaomi" : "xiaomi",
    "OTA" : "ota",
    "Config" : "config",
    "DevInfo" : "deviceinfo",
    "FSBrowser" : "fsbrowser"
  }
}
)=====";


void loadDeviceConfig(String fileName, JsonObject& deviceConfig) {
if (!SPIFFS.begin()) {
	 bootLog += "FS mount error\n";
	 Serial.print(bootLog);
}
  File configFile = SPIFFS.open(fileName, "r");
  String inJSON = configFile ? configFile.readString() : (String) defaultConfig;
  JsonObject& jsonCfg = jsonBuffer.parseObject(inJSON);
  if (!jsonCfg.success()) {
	bootLog += "Error parsing CONFIG file!\n";
    Serial.print(bootLog);
	JsonObject& jsonCfg = jsonBuffer.parseObject((String) defaultConfig);
	if (!jsonCfg.success()) {
		bootLog += "JSON parsing DEFAILT CONFIG. General Issue!\n";
		Serial.print(bootLog);
		return;
	}
		for (JsonObject::iterator it = jsonCfg.begin(); it != jsonCfg.end(); ++it) {
          deviceConfig[it->key] = it->value;
    }
		return;
	}

	for (JsonObject::iterator it = jsonCfg.begin(); it != jsonCfg.end(); ++it) {
          deviceConfig[it->key] = it->value;
    }
}

bool serviceIsEnabled(JsonObject& deviceConfig, String node) {
  if (!deviceConfig.containsKey(node) || deviceConfig.containsKey(node) && deviceConfig[node]["isEnabled"].as<bool>() == false) return false;
	JsonObject& nodeConfig = deviceConfig[node];
	if (nodeConfig.containsKey("workOnMode")) {
		String currmode = nodeConfig["workOnMode"].as<String>();
		if (currmode == "WIFIAP" && _WIFI_CLIENT_MODE == true) return false;
		if (currmode == "WIFICLIENT" && _WIFI_CLIENT_MODE == false) return false;
	}
  return true;
}


void parseBytes(const char* str, char sep, byte* bytes, int maxBytes, int base) {
    for (int i = 0; i < maxBytes; i++) {
        bytes[i] = strtoul(str, NULL, base);  // Convert byte
        str = strchr(str, sep);               // Find next separator
        if (str == NULL || *str == '\0') {
            break;                            // No more separators, exit
        }
        str++;                                // Point to next character after separator
    }
}

IPAddress convertIpFromString(String ipStr) {
	byte ip[4];
	parseBytes(const_cast<char *>((ipStr).c_str()), '.', ip, 4, 10);
	return IPAddress(ip[0],ip[1],ip[2],ip[3]);
}

String macToStr(const uint8_t* mac) {
String result;
for (int i = 0; i < 6; ++i) {
result += String(mac[i], 16);
if (i < 5)
result += ':';
}
return result;
}

bool writeLog(JsonObject& deviceConfig, String logName, String line, String formattedTime) {
		
	File f = SPIFFS.open(logName, logName == deviceConfig["General"]["log"] ? "w" :"a");
    int maxFileSize = deviceConfig["General"]["maxLogSize"].as<int>();
    int fileSize = f.size();
    if (fileSize >= maxFileSize * 1024) {
      f.close();
      f = SPIFFS.open(logName, "w");
    }
    if (!f) {
      Serial.println("file open failed " + logName);
    } else {
	//String formattedTime = timeClient.getFormattedTime();
	  f.print("[" + formattedTime + "] ");
      f.println(line);
      f.close();
    }
}

void mergeJson(JsonObject& dest, JsonObject& src) {
//https://github.com/bblanchon/ArduinoJson/wiki/Bag-of-Tricks
   for (auto kvp : src) {
     dest[kvp.key] = kvp.value;
   }
}