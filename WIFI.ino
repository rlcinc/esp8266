void WIFIStartAsAccessPoint(JsonObject& deviceConfig) {
  JsonObject& wifiConfig =  deviceConfig["WIFIAP"];
  
  IPAddress _APIP = convertIpFromString(wifiConfig["ip"].as<String>());

  WiFi.disconnect();
  String hostName = wifiConfig["ssid"].as<String>() + "-" + (String) ESP.getChipId();
  WiFi.hostname(hostName.c_str());
  WiFi.mode(WIFI_AP_STA);  //need both to serve the webpage and take commands via tcp
  WiFi.softAPConfig(_APIP, _APIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP(hostName.c_str(), wifiConfig["pass"]);

  delay(100);
  IPAddress myIP = WiFi.softAPIP();
  bootLog += "Started WIFI as AP. IP address: " + WiFi.localIP().toString() + "\n";
  //Serial.println(myIP);
  _WIFI_CLIENT_MODE = false;
}

void WIFIStartAsClient(JsonObject& deviceConfig) {
  JsonObject& wifiConfig =  deviceConfig["WIFI"];
  
  WiFi.mode(WIFI_STA);
  int n = WiFi.scanNetworks();
  bool isFound = false;
  for (int i = 0; i < n; ++i) {
    if (WiFi.SSID(i) == wifiConfig["ssid"].as<String>().c_str()) {
      isFound = true;
      break;
    }
  }
  
  if (!isFound) {
    WIFIStartAsAccessPoint(deviceConfig);
    return;
  }
  
  byte attempts = deviceConfig["General"]["attempts"].as<byte>();

  WiFi.begin(wifiConfig["ssid"].as<String>().c_str(), wifiConfig["pass"].as<String>().c_str());
  while (--attempts && WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(3000);
  }
  if (WiFi.status() != WL_CONNECTED) {
    WIFIStartAsAccessPoint(deviceConfig);
  } else {
    _WIFI_CLIENT_MODE = true;
    bootLog += "Started WIFI as Client. IP address: " + WiFi.localIP().toString() + "\n";
    //Serial.print("Started WIFI as Client. IP address: ");
    //Serial.println(WiFi.localIP());
  }
}

String getEncryptionType(uint8_t thisType) {
  if (thisType == 255) return "WPA2.Add";
  
  String types[] = {"WPA", "", "WPA2", "WEP", "", "None", "Auto"};
  return types[thisType - 2];
}
