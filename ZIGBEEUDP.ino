/*
 * /cgi-bin/luci/api/xqsystem/init_info
 * 
 * {"romversion":"2.15.78","countrycode":"CN","code":0,"id":"8799/00120085","routername":"WilderNet","inited":1,"connect":0,"routerId":"02638679-c6c5-464f-958b-fd16c97bda25","hardware":"R1CM","bound":0,"language":"zh_cn","modules":{"replacement_assistant":"1"}}
 * 
Connect to router http://192.168.0.1/cgi-bin/luci/api/xqsystem/login?username=admin&password=1ZaQ2ZaQ
{"url":"/cgi-bin/luci/;stok=4bd296d50e0d4ea34a5351e27eaa9409/web/home","token":"4bd296d50e0d4ea34a5351e27eaa9409","code":0}

get devicelist
http://192.168.0.1/cgi-bin/luci/;stok=4bd296d50e0d4ea34a5351e27eaa9409/api/misystem/devicelist
{
  "mac": "9C:2A:70:BC:6F:F1",
  "list": [
    {
      "mac": "5C:A3:9D:69:A2:27",
      "oname": "Unknown",
      "isap": 0,
      "parent": "",
      "authority": {
        "wan": 1,
        "pridisk": 0,
        "admin": 1,
        "lan": 0
      },
      "push": 0,
      "online": 1,
      "name": "Unknown",
      "times": 0,
      "ip": [
        {
          "downspeed": "267",
          "online": "3791",
          "active": 1,
          "upspeed": "871",
          "ip": "192.168.0.173"
        }
      ],
      "statistics": {
        "downspeed": "267",
        "online": "3791",
        "upspeed": "871"
      },
      "icon": "device_list_samsung.png",
      "type": 1
    },
    {
      "mac": "A0:20:A6:13:6C:FE",
      "oname": "ESP_136CFE",
      "isap": 0,
      "parent": "",
      "authority": {
        "wan": 1,
        "pridisk": 0,
        "admin": 1,
        "lan": 0
      },
      "push": 0,
      "online": 1,
      "name": "ESP_136CFE",
      "times": 0,
      "ip": [
        {
          "downspeed": "0",
          "online": "3020",
          "active": 1,
          "upspeed": "0",
          "ip": "192.168.0.160"
        }
      ],
      "statistics": {
        "downspeed": "0",
        "online": "3020",
        "upspeed": "0"
      },
      "icon": "",
      "type": 1
    },
    {
      "mac": "F0:B4:29:CC:7A:F6",
      "oname": "lumi-gateway-v3_miio47409387",
      "isap": 0,
      "parent": "",
      "authority": {
        "wan": 1,
        "pridisk": 0,
        "admin": 1,
        "lan": 0
      },
      "push": 0,
      "online": 1,
      "name": "lumi-gateway-v3_miio47409387",
      "times": 0,
      "ip": [
        {
          "downspeed": "0",
          "online": "489179",
          "active": 1,
          "upspeed": "0",
          "ip": "192.168.0.116"
        }
      ],
      "statistics": {
        "downspeed": "0",
        "online": "489179",
        "upspeed": "0"
      },
      "icon": "device_list_mi.png",
      "type": 1
    },
    {
      "mac": "B0:E2:35:9F:42:0E",
      "oname": "RedmiNote3-Redmi",
      "isap": 0,
      "parent": "",
      "authority": {
        "wan": 1,
        "pridisk": 0,
        "admin": 1,
        "lan": 0
      },
      "push": 0,
      "online": 1,
      "name": "RedmiNote3-Redmi",
      "times": 0,
      "ip": [
        {
          "downspeed": "351",
          "online": "836",
          "active": 1,
          "upspeed": "1206",
          "ip": "192.168.0.198"
        }
      ],
      "statistics": {
        "downspeed": "351",
        "online": "836",
        "upspeed": "1206"
      },
      "icon": "device_list_mi.png",
      "type": 1
    },
    {
      "mac": "9C:2A:70:BC:6F:F1",
      "oname": "Dima-pc",
      "isap": 0,
      "parent": "",
      "authority": {
        "wan": 1,
        "pridisk": 0,
        "admin": 1,
        "lan": 1
      },
      "push": 0,
      "online": 1,
      "name": "Dima-pc",
      "times": 0,
      "ip": [
        {
          "downspeed": "0",
          "online": "4528",
          "active": 1,
          "upspeed": "0",
          "ip": "192.168.0.101"
        }
      ],
      "statistics": {
        "downspeed": "0",
        "online": "4528",
        "upspeed": "0"
      },
      "icon": "",
      "type": 1
    },
    {
      "mac": "78:40:E4:B4:66:71",
      "oname": "android-308d0ce8a0ad96d3",
      "isap": 0,
      "parent": "",
      "authority": {
        "wan": 1,
        "pridisk": 0,
        "admin": 1,
        "lan": 0
      },
      "push": 0,
      "online": 1,
      "name": "android-308d0ce8a0ad96d3",
      "times": 0,
      "ip": [
        {
          "downspeed": "0",
          "online": "64482",
          "active": 1,
          "upspeed": "0",
          "ip": "192.168.0.167"
        }
      ],
      "statistics": {
        "downspeed": "0",
        "online": "64482",
        "upspeed": "0"
      },
      "icon": "device_list_samsung.png",
      "type": 1
    },
    {
      "mac": "64:D9:54:E4:63:5F",
      "oname": "Unknown",
      "isap": 0,
      "parent": "",
      "authority": {
        "wan": 1,
        "pridisk": 0,
        "admin": 1,
        "lan": 0
      },
      "push": 0,
      "online": 1,
      "name": "Unknown",
      "times": 0,
      "ip": [
        {
          "downspeed": "0",
          "online": "489265",
          "active": 1,
          "upspeed": "0",
          "ip": "192.168.0.152"
        },
        {
          "downspeed": "0",
          "online": "489265",
          "active": 1,
          "upspeed": "0",
          "ip": "192.168.0.153"
        }
      ],
      "statistics": {
        "downspeed": "0",
        "online": "489265",
        "upspeed": "0"
      },
      "icon": "",
      "type": 1
    },
    {
      "mac": "64:89:9A:86:12:EB",
      "oname": "android-d5c67de39b79722",
      "isap": 0,
      "parent": "",
      "authority": {
        "wan": 1,
        "pridisk": 0,
        "admin": 1,
        "lan": 1
      },
      "push": 0,
      "online": 1,
      "name": "android-d5c67de39b79722",
      "times": 0,
      "ip": [
        {
          "downspeed": "0",
          "online": "921",
          "active": 1,
          "upspeed": "0",
          "ip": "192.168.0.179"
        }
      ],
      "statistics": {
        "downspeed": "0",
        "online": "921",
        "upspeed": "0"
      },
      "icon": "device_list_lg.png",
      "type": 1
    }
  ],
  "code": 0
}
  

1. 224.0.0.50 : 4321 -> {"cmd":"whois"}
{"cmd":"iam","port":"9898","sid":"f0b429cc7af6","model":"gateway","ip":"192.168.0.116"}

get_id_list
2. 192.168.0.116 : 9898 -> {"cmd" : "get_id_list"}
{"cmd":"get_id_list_ack","sid":"f0b429cc7af6","token":"QcPqR6VVwb6QJyx8","data":"[\\"158d0001530f60\\",\\"158d000155cb91\\",\\"158d00015311ae\\",\\"158d000155c057\\"]"}

READ
3. 192.168.0.116 : 9898 -> {"cmd":"read","sid":"158d0001530f60"}
{"cmd":"read_ack","model":"sensor_ht","sid":"158d0001530f60","short_id":17367,"data":"{\\"voltage\\":2995,\\"temperature\\":\\"2452\\",\\"humidity\\":\\"5339\\"}"}
{"cmd":"read_ack","model":"gateway","sid":"f0b429cc7af6","short_id":0,"data":"{\\"rgb\\":0,\\"illumination\\":1292,\\"proto_version\\":\\"1.0.6\\"}"}


/* {"cmd" : "get_id_list"}
   {"cmd":"get_id_list_ack","sid":"f0b429cc7af6","token":"hyT2AdRnOJjHmFkw","data":"[\\"158d0001530f60\\",\\"158d000155cb91\\",\\"158d00015311ae\\"]"}

   {"cmd":"read","sid":"f0b429cc7af6"}
   {"cmd":"read_ack","model":"gateway","sid":"f0b429cc7af6","short_id":0,"data":"{\\"rgb\\":0,\\"illumination\\":1292,\\"proto_version\\":\\"1.0.6\\"}"}

   {"cmd":"heartbeat","model":"sensor_ht","sid":"158d00015311ae","short_id":52028},
*/

unsigned char const AES_KEY_IV [16] = {0x17, 0x99, 0x6d, 0x09, 0x3D, 0x28, 0xdd, 0xb3, 0xBA, 0x69, 0x5A, 0x2E, 0x6F, 0x58, 0x56, 0x2e};

void zigbeeUdp_init(JsonObject& deviceConfig) {
  if (!serviceIsEnabled(deviceConfig, "ZIGBEEUDP")) return;

  JsonObject& zbConfig =  deviceConfig["ZIGBEEUDP"];
  IPAddress serverIP = convertIpFromString(zbConfig["ip"].as<String>());
  zigbeeUdp.beginMulticast(WiFi.localIP(), serverIP, zbConfig["localPort"].as<uint32_t>());
  Serial.println("Started zigbeeUdp");
  
  pinMode(ONBOARDLED, OUTPUT); // Onboard LED
  digitalWrite(ONBOARDLED, HIGH); // Switch off LED
}

void zigbeeUdp_loop(JsonObject& deviceConfig) {
  if (!serviceIsEnabled(deviceConfig, "ZIGBEEUDP")) return;
  JsonObject& zbConfig =  deviceConfig["ZIGBEEUDP"];
  
  char incomingPacket[512];
  int packetSize = zigbeeUdp.parsePacket();
  if (packetSize) {
    int len = zigbeeUdp.read(incomingPacket, 512);
    if (len > 0) incomingPacket[len] = 0;
    String formattedTime = NTP.getTimeDateString();
    Serial.print("[" + formattedTime + "] ");
    Serial.printf("%s\n", incomingPacket);

    writeLog(deviceConfig, zbConfig["log"].as<String>(), incomingPacket, formattedTime);
    digitalWrite(ONBOARDLED, LOW); // Turn on LED
    delay(100);
    digitalWrite(ONBOARDLED, HIGH); // Turn off LED
  }
}
