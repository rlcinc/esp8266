# README #

esp8266 FileManager with all standard functions such as
- Create file
- Delete File
- Rename File
- Update File. Also implemented smart method for automatic gzip/gunzip text files. Need for reduce file size on ESP disk

- ESP can work in two modes : AP, Client.

![picture](img/desktop.png)
![picture](img/mobile.png)